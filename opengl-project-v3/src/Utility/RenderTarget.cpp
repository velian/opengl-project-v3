#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include "Utility/RenderTarget.h"
#include "Scene/Camera/Camera.h"
#include "Utility/ShaderLoader.h"

#include "Application.h"

RenderTarget::RenderTarget()
{
	
}

void RenderTarget::SetProgram(unsigned int _programID)
{
	m_programID = _programID;
}

void RenderTarget::SetProgram(std::vector<std::string> _shaders)
{
	m_programID = ShaderLoader::GetInstance()->LoadShaders(_shaders);
}


void RenderTarget::Create()
{
	m_shaderData = new ShaderData();

	m_position = glm::vec2(0, 0);
	m_dimensions = glm::vec2(1, 1);

	m_programID = 0;

	float vertexData[] =
	{
		m_position.x + -m_dimensions.x, -m_dimensions.y, 0, 1, 0, 0,
		m_position.x + m_dimensions.x, -m_dimensions.y, 0, 1, 1, 0,
		m_position.x + m_dimensions.x, m_dimensions.y, 0, 1, 1, 1,
		m_position.x + -m_dimensions.x, m_dimensions.y, 0, 1, 0, 1,
	};

	unsigned int indexData[] =
	{
		0, 1, 2,
		0, 2, 3,
	};

	//Setup Frame Buffer
	glGenFramebuffers(1, &m_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

	//Texture shtuff
	glGenTextures(1, &m_FBOTexture);
	glBindTexture(GL_TEXTURE_2D, m_FBOTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, Application::GetInstance()->GetWidth(), Application::GetInstance()->GetHeight());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_FBOTexture, 0);

	glGenRenderbuffers(1, &m_RBO);
	glBindRenderbuffer(GL_RENDERBUFFER, m_RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, Application::GetInstance()->GetWidth(), Application::GetInstance()->GetHeight());
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_RBO);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		printf("Framebuffer Error!\n");
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glGenVertexArrays(1, &m_shaderData->m_VAO);
	glBindVertexArray(m_shaderData->m_VAO);

	glGenBuffers(1, &m_shaderData->m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData->m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_shaderData->m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData->m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RenderTarget::SetActive()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);
	glViewport(0, 0, Application::GetInstance()->GetWidth(), Application::GetInstance()->GetHeight());
}

void RenderTarget::ClearActive()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, Application::GetInstance()->GetWidth(), Application::GetInstance()->GetHeight());
}