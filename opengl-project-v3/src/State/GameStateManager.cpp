#include "State/GameStateManager.h"
#include "State/GameState.h"

#include "State/Examples/Example1.h"
#include "State/Examples/Example2.h"
#include "State/Test.h"

#include "Application.h"

GameStateManager* GameStateManager::m_instance = nullptr;

GameStateManager* GameStateManager::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new GameStateManager();
	}
	return m_instance;
}

void GameStateManager::Initialize(unsigned int _gameState)
{
	m_currentState = _gameState;
	m_previousState = m_currentState;

	AddState(new Example1(0));
	AddState(new Example2(1));
	//AddState(new Test(0));
}

void GameStateManager::AddState(GameState* _gameState)
{
	m_stateList.push_back(_gameState);
}

void GameStateManager::SetState(unsigned int _gameState)
{
	if (m_currentState < m_stateList.size())
	{
		m_previousState = m_currentState;
		m_currentState = _gameState;
	}
	else
	{
		printf("Attempted to load NonExistant GameState : %i", _gameState);
	}
}

void GameStateManager::NextState()
{
	if (m_currentState < m_stateList.size() - 1)
	{
		m_previousState = m_currentState;
		m_currentState = GetState() + 1;
	}
}

void GameStateManager::PreviousState()
{
	if (m_currentState > 0)
	{
		m_previousState = m_currentState;
		m_currentState = GetState() - 1;
	}
}

unsigned int GameStateManager::GetState()
{
	return m_currentState;
}

unsigned int GameStateManager::GetStateSize()
{
	return m_stateList.size();
}

void GameStateManager::Update()
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{
			m_stateList[i]->Update(Application::GetInstance()->GetDeltaTime());
			return;
		}
	}
}

void GameStateManager::Draw()
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{
			m_stateList[i]->Draw();
			return;
		}
	}
}