#include <GLFW/glfw3.h>
#include <GL/gl_core_4_4.h>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include <stdio.h>

#include "Utility\Gizmos.h"
#include "State\GameStateManager.h"

#include "Application.h"

Application* Application::m_instance = nullptr;

Application* Application::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Application();
	}
	return m_instance;
}

void Application::Initialize(int _argc, char** _argv, unsigned int _width, unsigned int _height, char* _windowTitle)
{
	m_width = _width;
	m_height = _height;
	m_windowTitle = _windowTitle;

	m_deltaTime = 0;
	m_previousTime = 0;
	m_currentTime = 0;

	if (glfwInit() == false)
	{
		printf("Failed to initialize GLFW\n");
		return;
	}

	m_window = glfwCreateWindow(m_width, m_height, m_windowTitle, nullptr, nullptr);

	if (m_window == nullptr)
	{
		printf("Window failed to initialize\n");
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		printf("OGL failed to initialize\n");
		glfwTerminate();
		return;
	}

	GameStateManager::GetInstance()->Initialize(0);

	for (int i = 0; i < _argc; i++)
	{
		std::string argv = _argv[i];

		if (argv.find("-server") != std::string::npos)
		{
			GameStateManager::GetInstance()->SetState(1);
		}
	}

	Gizmos::create(0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF);
	ImGui_ImplGlfwGL3_Init(m_window, true);
}

void Application::Initialize(int _argc, char** _argv, glm::vec2 _windowDimensions, char* _windowTitle)
{
	Initialize(_argc, _argv, (unsigned int)_windowDimensions.x, (unsigned int)_windowDimensions.y, _windowTitle);
}

void Application::Run()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	

	while (glfwWindowShouldClose(m_window) == false && m_running)
	{
		Gizmos::clear();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.5f, 0.5f, 0.5f, 1.f);
		glfwPollEvents();

		UpdateDeltaTime();

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glCullFace(GL_BACK);

		ImGui_ImplGlfwGL3_NewFrame();

		ImGui::Text("%.3f ms/frame : (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Separator();

		GameStateManager::GetInstance()->Update();
		GameStateManager::GetInstance()->Draw();

		ImGui::Render();

		glfwSwapBuffers(m_window);

		if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE, false) == true)
		{
			m_running = false;
		}
	}

	delete this;
}

double Application::GetDeltaTime()
{
	return m_deltaTime;
}

void Application::UpdateDeltaTime()
{
	m_currentTime = (float)glfwGetTime();
	m_deltaTime = m_currentTime - m_previousTime;
	m_previousTime = m_currentTime;
}

unsigned int Application::GetWidth()
{
	return m_width;
}

unsigned int Application::GetHeight()
{
	return m_height;
}

Application::~Application()
{
	ImGui_ImplGlfwGL3_Shutdown();
	glfwDestroyWindow(m_window);
	glfwTerminate();
}