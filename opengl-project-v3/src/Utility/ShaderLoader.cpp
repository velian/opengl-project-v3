#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include "Utility/ShaderLoader.h"

ShaderLoader* ShaderLoader::m_instance = nullptr;

ShaderLoader* ShaderLoader::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new ShaderLoader();
	}
	return m_instance;
}

unsigned int ShaderLoader::LoadShaders(std::vector<std::string> _shaders, char** _varyings)
{
	std::vector<unsigned int> shaders;

	for (unsigned int i = 0; i < _shaders.size(); i++)
	{
		shaders.push_back(LoadShader((char*)_shaders[i].c_str()));
	}

	return CreateProgram(shaders, _varyings);
}

unsigned int ShaderLoader::LoadShader(std::string _shader)
{
	int success = GL_FALSE;
	unsigned char* source = FileToBuffer(_shader.c_str());

	unsigned int handle;

	if (_shader.substr(_shader.find_last_of(".") + 1) == "vs")
	{
		handle = glCreateShader(GL_VERTEX_SHADER);
	}
	else if (_shader.substr(_shader.find_last_of(".") + 1) == "fs")
	{
		handle = glCreateShader(GL_FRAGMENT_SHADER);
	}
	else if (_shader.substr(_shader.find_last_of(".") + 1) == "gs")
	{
		handle = glCreateShader(GL_GEOMETRY_SHADER);
	}

	if (source == NULL){
		printf("Failed to load a shader with the path : %s\n", _shader.c_str());
		return 0;
	}

	glShaderSource(handle, 1, (const char**)&source, 0);
	glCompileShader(handle);

	glGetShaderiv(handle, GL_COMPILE_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetShaderInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to compile shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	delete[] source;

	return handle;

}

unsigned char* ShaderLoader::FileToBuffer(std::string _filePath)
{
	FILE* file = fopen(_filePath.c_str(), "rb");

	if (file == nullptr)
	{
		printf("Couldn't open file : %s", _filePath);
		return nullptr;
	}

	// get number of bytes in file
	fseek(file, 0, SEEK_END);
	unsigned int uiLength = (unsigned int)ftell(file);
	fseek(file, 0, SEEK_SET);

	// allocate buffer and read file contents
	unsigned char* acBuffer = new unsigned char[uiLength + 1];
	memset(acBuffer, 0, uiLength + 1);
	fread(acBuffer, sizeof(unsigned char), uiLength, file);

	fclose(file);
	return acBuffer;
}

unsigned int ShaderLoader::CreateProgram(std::vector<unsigned int> _shaders, char** _varyings)
{
	int success = GL_FALSE;

	unsigned int handle = glCreateProgram();

	for (unsigned int i = 0; i < _shaders.size(); i++)
	{
		if (_shaders[i] != NULL)
		{
			glAttachShader(handle, _shaders[i]);
		}
	}

	if (_varyings != nullptr)
	{
		glTransformFeedbackVaryings(handle, 4, _varyings, GL_INTERLEAVED_ATTRIBS);
	}

	glLinkProgram(handle);
	glGetProgramiv(handle, GL_LINK_STATUS, &success);

	if (success == GL_FALSE)
	{
		int infoLength = 0;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &infoLength);
		char* infoLog = new char[infoLength];

		glGetProgramInfoLog(handle, infoLength, 0, infoLog);
		printf("Faliled to link shader program : %s", infoLog);
		delete[] infoLog;

		return 0;
	}

	return handle;
}