#pragma once

#include <vector>
#include <functional>
#include <GLM\glm.hpp>

#include "Texture.h"

#define GOOEY_VER 0.1
#ifndef GOOEY_API
#define GOOEY_API
#endif

namespace Gooey
{
	class GooeyObject
	{
	public:
		glm::vec2 m_position, m_scale;
		int m_drawOrder;
		Texture* m_texture;
		int m_shader;
		bool m_enabled;
	};

	class Utility
	{
	public:

		static Utility* Instance();

		std::vector<GooeyObject*> m_drawList;

	private:

		Utility();
		Utility(const Utility*&) = delete;

		static Utility* m_instance;
	};

	GOOEY_API bool		Initialize();
	GOOEY_API void		Begin();
	GOOEY_API void		End();

	GOOEY_API void		Image(glm::vec2 _position, glm::vec2 _scale);
	GOOEY_API bool		Button(glm::vec2 _position, glm::vec2 _scale);

	
};
