#ifndef FBX_OBJECT_H
#define FBX_OBJECT_H

#include "FBX/FBXFile.h"
#include "Scene/Shader.h"

class FreeCamera;
class GameState;
class Light;

class FBXObject
{
public:

	FBXObject();

	void Initialize();

	void Create(const char* a_filename, FBXFile::UNIT_SCALE a_scale = FBXFile::UNITS_METER, bool a_loadTextures = true, bool a_loadAnimations = true, bool a_flipTextureY = true);

	virtual void Update(float _deltaTime);
	virtual void Draw(FreeCamera* _camera);

	FBXFile* GetFile(){ return m_file; }

	Light* GetLight(){ return m_light; }
	void SetLight(Light* _light){ m_light = _light; }

private:

	FBXFile* m_file;
	Shader* m_shader;
	Light* m_light;
	float m_timer;
};

#endif