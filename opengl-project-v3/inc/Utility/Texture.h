#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <iomanip>
#include <Windows.h>
#include <Commdlg.h>
#include <algorithm>
#include <string>
#include <vector>

enum DRAW_TYPE
{
	NEAREST = 0,
	LINEAR = 1
};

enum TEX_TYPE
{
	PNG = 0,
	JPG = 1,
	TGA = 2
};

class Camera;
class Texture
{
public:

	Texture();
	Texture(std::string _filePath, DRAW_TYPE _drawType = LINEAR);
	Texture(std::string _filePath, int _frameWidth, int _frameHeight, float _startTime = 0.25f, DRAW_TYPE _drawType = LINEAR);

	void Initialize(std::string _filePath, DRAW_TYPE _drawType = DRAW_TYPE::LINEAR);
	void Initialize(std::string _filePath, int _frameWidth, int _frameHeight, float _startTime = 0.25f, DRAW_TYPE _drawType = DRAW_TYPE::LINEAR);

	void LoadCubeMap(std::vector<std::string> _faces);

	virtual void Update(double _deltaTime);

	unsigned int GetID(){ return m_id; }
	std::string GetCurrentPath() { return m_currentPath; }

	unsigned int m_id;

	int m_currentFrame;
	int m_maxFrames;
	int m_frameWidth, m_frameHeight;
	int m_imageWidth, m_imageHeight;

	glm::vec2 m_uv;

private:

	std::string m_currentPath;

	unsigned char* m_data;

	bool m_spriteSheet;

	double m_startTime;
	double m_timer;

	int m_imageFormat;
};

#endif