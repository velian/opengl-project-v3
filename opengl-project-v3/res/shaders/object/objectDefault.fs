#version 410

in vec3 vPosition;
in vec3 vNormal;
in vec2 vUV;

out vec4 FragColor;

uniform bool HasTextures;
uniform sampler2D TextureList[4];

uniform bool HasLight;
uniform vec3 LightDir; 
uniform vec3 LightColour;

void main()
{
	//vec2 coords = vUV;
	//coords *= 10;

	if(HasTextures)
	{
		for(int i = 0; i < TextureList.length(); i++)
		{
			if(i == 0)
			{
				FragColor = texture(TextureList[0], vUV) * vec4(1,1,1,1);
			}
			else
			{
				FragColor *= texture(TextureList[0], vUV);
			}
		}
	}

	if(HasLight)
	{
		float d = max(0, dot(normalize(vNormal.xyz), LightDir));
		FragColor *= vec4(LightColour * d, 1);
	}
}