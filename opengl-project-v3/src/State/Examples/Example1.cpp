#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "State/Examples/Example1.h"
#include "State/GameStateManager.h"
#include "Scene/CheckerBoard.h"
#include "Scene/Camera/Camera.h"
#include "Input/JoystickManager.h"

#include "Utility/Gizmos.h"
#include "Utility/Client.h"

#include "Utility/SoundManager.h"
//#include "Utility/Gooey.h"

Example1::Example1(unsigned int _id) : GameState(_id)
{
	m_camera = new Camera(3.14159f * 0.25f, 4.0f / 3.0f, 0.01, 100);
	m_camera->setSpeed(0);
	m_camera->setLookAtFrom(glm::vec3(0, 12, 3.5), glm::vec3(0, 0, 3.5));
	m_checkerBoard = new CheckerBoard();

	m_network = new Client();

	m_clientName = DEFAULT_NAME;
	m_serverIP = DEFAULT_IP;
	m_serverPort = DEFAULT_PORT;

	//Load the sounds for the state
	SoundManager::Instance()->LoadSound("./sounds/music.mp3", "music");
	SoundManager::Instance()->LoadSound("./sounds/back.wav", "back");
	SoundManager::Instance()->LoadSound("./sounds/select.wav", "select");
	SoundManager::Instance()->LoadSound("./sounds/validate.wav", "validate");

	//Gooey::Initialize();

	//Play game music
	
	#ifndef _DEBUG
		SoundManager::Instance()->PlayOneShot("music", true);
	#endif
}

void Example1::Update(double _deltaTime)
{
	m_camera->update(_deltaTime);

	if (m_network->Initialized() && m_checkerBoard->m_currentPlayer == m_network->m_id)
	{
		m_checkerBoard->Update(_deltaTime, m_camera, m_network);
	}

	if (m_network->Initialized())
	{	
		m_network->Update(_deltaTime, m_checkerBoard);
	}

	if (ImGui::GetConsoleUpdated())
	{
		if (m_network->Initialized())
			m_network->SendClientMessage(ImGui::GetConsoleBuffer());
	}

	if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE))
	{
		m_network->EndConnection();
	}
}

void Example1::Draw()
{
	DrawGUI("EXAMPLE 1");

	m_checkerBoard->Draw(m_camera);

	ImGui::ShowCustomConsole();

	Gizmos::draw(m_camera->getProjectionView());
}

void Example1::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);

	if (ImGui::TreeNode("Network Options"))
	{		
		if (ImGui::Button("Start Client"))
		{
			if (!m_network->Initialized())
			{
				if (!m_clientName.empty())
					m_network->InitializeClient((char*)m_clientName.c_str(), m_serverPort, (char*)m_serverIP.c_str());
				else
					m_network->InitializeClient(DEFAULT_NAME, m_serverPort, (char*)m_serverIP.c_str());
			}
		}

		ImGui::InputText("Player Name", (char*)m_clientName.c_str(), 256);
		ImGui::InputText("Server IP", (char*)m_serverIP.c_str(), 64);
		ImGui::InputInt("Server Port", (int*)&m_serverPort);

		char* clientList[MAX_CLIENTS];

		for (int i = 0; i < m_network->m_clientList.size(); i++)
		{
			clientList[i] = m_network->m_clientList[i];
		}

		int currentNumber = 0;

		if (!m_network->m_clientList.empty())
		{
			ImGui::ListBox((const char*)"Player List", &currentNumber, (const char**)clientList, (int)m_network->m_clientList.size());
		}

		ImGui::TreePop();
	}
}