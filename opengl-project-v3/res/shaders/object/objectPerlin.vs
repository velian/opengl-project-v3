#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 UV;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vUV;

uniform mat4 ProjectionView;
uniform mat4 Transform;

uniform bool HasPerlin;
uniform sampler2D perlinTexture;

void main()
{
	vPosition = Position;
	vNormal = Normal.xyz;
	vUV = UV;

	if(HasPerlin)
	{
		float offset = texture(perlinTexture, vUV.xy).r;
		vPosition += vNormal * offset;
	}

	gl_Position = Transform * vec4(vPosition, 1);
}