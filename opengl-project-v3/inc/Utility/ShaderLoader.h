#ifndef SHADER_LOADER_H
#define SHADER_LOADER_H

#include <vector>

struct ShaderData
{
	ShaderData()
	{
		m_VAO = 0;
		m_VBO = 0;
		m_IBO = 0;
	}

	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
};

class ShaderLoader
{
public:

	unsigned int LoadShaders(std::vector<std::string> _shaderList, char** _varyings = nullptr);

	static ShaderLoader* GetInstance();

private:

	unsigned int LoadShader(std::string _shaders);
	unsigned int CreateProgram(std::vector<unsigned int> _shaders, char** varyings = nullptr);

	unsigned char* FileToBuffer(std::string _filePath);

	ShaderLoader(){}
	ShaderLoader(const ShaderLoader*&) = delete;

	static ShaderLoader* m_instance;

};

#endif SHADER_LOADER_H