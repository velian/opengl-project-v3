#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <IMGUI/imgui.h>

class GameState
{
public:

	GameState(unsigned int _id);

	virtual void Update(double _deltaTime);
	virtual void Draw();

	unsigned int m_id;

protected:
	virtual void DrawGUI(char* _stateName = "DEBUG STATE");
};

#endif GAMESTATE_H