#ifndef APPLICATION_H
#define APPLICATION_H

#include <GLM/glm.hpp>

struct GLFWwindow;
class Application
{
public:

	void Initialize(int _argc, char** _argv, glm::vec2 _windowDimensions, char* _windowTitle = "Test GL");
	void Initialize(int _argc, char** _argv, unsigned int _width = 1280, unsigned int _height = 720, char* _windowTitle = "Test GL");
	
	void Run();

	double GetDeltaTime();

	unsigned int GetWidth();
	unsigned int GetHeight();

	static Application* GetInstance();

private:

	Application(){};
	Application(Application const&) = delete;
	~Application();

	void UpdateDeltaTime();

	static Application* m_instance;

	GLFWwindow* m_window;

	bool m_running;
	char* m_windowTitle;
	unsigned int m_width, m_height;

	double m_deltaTime;
	double m_currentTime;
	double m_previousTime;
};

#endif APPLICATION_H