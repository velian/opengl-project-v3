#pragma once

#include "FMOD\fmod.hpp"

class Sound
{
public:

	Sound(char* _filePath, char* _soundName);

	char* m_name;
	FMOD::Sound* m_sound;	
};