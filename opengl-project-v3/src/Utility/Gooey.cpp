#include <GLFW\glfw3.h>

#include "Utility/Texture.h"
#include "Utility/Gooey.h"

#include "Utility/ShaderLoader.h"

Gooey::Utility* Gooey::Utility::m_instance = nullptr;

Gooey::Utility* Gooey::Utility::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Gooey::Utility();
	}
	return m_instance;
}

Gooey::Utility::Utility()
{
	m_drawList.clear();
}

bool Gooey::Initialize()
{
	if (GLFW_NOT_INITIALIZED)
	{
		printf("Initialize GLFW before using the Gooey API");
		return false;
	}

	Gooey::Utility::Instance();
}

void Gooey::Begin()
{
	
}

void Gooey::End()
{

}

void Gooey::Image(glm::vec2 _position, glm::vec2 _scale)
{
	GooeyObject* object = new GooeyObject();
	object->m_enabled = true;
	object->m_position = glm::vec2(0);
	object->m_scale = glm::vec2(1);
	object->m_drawOrder = 1;

	object->m_texture->Initialize("./images/vignette.png");

	std::vector<std::string> shaderList;
	shaderList.push_back("./shader/objectDefault.vs");
	shaderList.push_back("./shader/objectDefault.fs");

	object->m_shader = ShaderLoader::GetInstance()->LoadShaders(shaderList);

	Utility::Instance()->m_drawList.push_back(new GooeyObject());
}

bool Gooey::Button(glm::vec2 _position, glm::vec2 _scale)
{
	return false;
}