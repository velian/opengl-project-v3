#pragma once

#include <GLM/glm.hpp>

class Camera;
class BoardTile;
class Client;
class BoardPiece
{
public:

	enum PieceColour
	{
		RED,
		BLACK,
		NONE
	};

	BoardPiece(int _playerNumber)
	{
		m_pieceColour = (PieceColour)_playerNumber;
		m_lerpTimer = 0;
		m_king = false;
		m_enabled = true;
	}
	~BoardPiece();

	void Initialize(glm::vec3 _position, float _radius, int _segments);

	void Update(double _deltaTime, Camera* _camera);
	void Draw(Camera* _camera);
	
	PieceColour m_pieceColour;

	glm::vec3 m_position;
	glm::vec3 m_originPosition;
	
	BoardTile* m_parent;

	glm::vec2 m_index;

	float m_lerpTimer;
	float m_radius;
	int m_segments;

	bool m_king;
	bool m_enabled;
};