#pragma once

#include <GLM/glm.hpp>
#include <vector>

#include "Scene/CheckerBoard.h"

#define NE glm::vec2( 1, 1)
#define NW glm::vec2(-1, 1)
#define SE glm::vec2( 1,-1)
#define SW glm::vec2(-1,-1)

class Camera;
class BoardPiece;
class Client;
class BoardTile
{
public:

	enum BoardColour
	{
		WHITE,
		BLACK,
		SELECTED,
		AVAILABLE,
		NONE
	};

	enum MoveDir
	{
		NorthEast,//red
		NorthWest,//red
		SouthEast,//black
		SouthWest //black
	};

	struct MoveInfo
	{
		MoveInfo(BoardTile* _endTile, BoardTile* _jumpedTile = nullptr) : endTile(_endTile), jumpTile(_jumpedTile){}
		BoardTile* endTile;
		BoardTile* jumpTile;
		MoveDir moveDirection;
	};

	BoardTile();
	BoardTile(BoardColour _tileType) : m_boardColour(_tileType)
	{
		m_child = nullptr;
		m_parent = nullptr;
		m_index = glm::vec2(-1, -1);
	}

	void GenerateNeighbors();

	void Update(double _deltaTime, Camera* _camera, Client* _client);
	void Draw(Camera* m_camera);	

	void MovePiece(double _deltaTime, BoardTile* _boardTile, Client* _client);

	std::vector<MoveInfo*> m_neighbors;

	CheckerBoard* m_parent;	
	BoardPiece* m_child;

	BoardColour m_boardColourOrigin;
	BoardColour m_boardColour;

	glm::vec2 m_index;

	glm::vec3 m_position;
	glm::vec3 m_scale;
};