#ifndef GPUPARTICLE_EMITTER_H
#define GPUPARTICLE_EMITTER_H

#include <GLM/glm.hpp>

#include "Utility/Texture.h"
#include "Scene/Renderable/GPUParticle.h"
#include "Utility/ShaderLoader.h"

using namespace glm;
class Camera;
class GPUParticleEmitter
{
public:

	GPUParticleEmitter(vec3 _position = vec3(0, 0, 0), Texture* _texture = nullptr, float _startSize = 0.1f, float _endSize = 0.2f);
	void Initialize(vec3 _position, Texture* _texture, float _startSize, float _endSize);

	virtual void Update(double _deltaTime);
	virtual void Draw(Camera* _camera);
	void DrawGUI(char* _emitterName);

protected:

	void Create();
	void CreateUpdateShader();
	void CreateDrawShader();

	//Variables//
	GPUParticle* m_particles;

	unsigned int m_updateShader;
	unsigned int m_drawShader;

	ShaderData* m_shaderData;

	Texture* m_texture;

	vec3 m_position;

	vec4 m_startColour;
	vec4 m_endColour;

	float m_emitTimer;
	float m_emitRate;

	float m_lifeSpanMin;
	float m_lifeSpanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	unsigned int m_activeBuffer;

	float m_lastDrawTime;
};

#endif