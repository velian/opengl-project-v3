#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include <GLM/glm.hpp>
#include <vector>

struct ShaderData;
class Camera;
class RenderTarget
{
public:

	RenderTarget();

	void SetProgram(unsigned int _programID);
	void SetProgram(std::vector<std::string> _shaders);

	void SetActive();
	void ClearActive();

protected:

	void Create();

	virtual void Update(){}
	virtual void Draw(){}

	ShaderData* m_shaderData;

	glm::vec2 m_dimensions;
	glm::vec2 m_position;

	unsigned int m_programID;
	unsigned int m_FBO;
	unsigned int m_RBO;
	unsigned int m_FBOTexture;

};

#endif RENDERTARGET_H