#ifndef GAMESTATEMANAGER_H
#define GAMESTATEMANAGER_H

#include <vector>

class GameState;
class GameStateManager
{
public:

	void AddState(GameState* _gameState);
	void SetState(unsigned int _gameState);
	
	void Initialize(unsigned int _gameState = 0);

	void NextState();
	void PreviousState();

	unsigned int GetState();
	unsigned int GetStateSize();

	void Update();
	void Draw();

	static GameStateManager* GetInstance();

private:

	GameStateManager(){}
	GameStateManager(const GameStateManager&) = delete;

	unsigned int m_currentState;
	unsigned int m_previousState;

	std::vector<GameState*> m_stateList;

	static GameStateManager* m_instance;

};

#endif GAMESTATEMANAGER_H