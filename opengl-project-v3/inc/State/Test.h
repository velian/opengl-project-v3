#ifndef TEST_H
#define TEST_H

#include "State/GameState.h"

struct Light;
class Camera;
class PlayerCamera;
class Object;
class RTOverlay;
class Overlay;
class Skybox;
class GPUParticleEmitter;
class Test : public GameState
{
public:

	Test(unsigned int _id);

	virtual void Update(double _deltaTime);
	virtual void Draw();

private:
	//GameState Objects
	PlayerCamera* m_playerCamera;
	Camera* m_camera;
	Object* m_object;

	//Mega Temporary!
	Object* m_objects;

	RTOverlay* m_rtOverlay;
	Overlay* m_overlay;
	Skybox* m_skybox;
	Light* m_light;
	GPUParticleEmitter* m_emitter;
	Object* m_player;
	//

	bool m_visualizeAllLayers;
};

#endif EXAMPLE2_h