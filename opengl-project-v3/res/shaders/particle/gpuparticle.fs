#version 410

in vec4 Colour;
in vec2 vTexCoords;

out vec4 FragColour;

uniform sampler2D tex;

void main()
{
	FragColour = texture(tex, vTexCoords) * Colour;

	//if(FragColour.a < 0.5f)
	//{
	//	discard;
	//}
}