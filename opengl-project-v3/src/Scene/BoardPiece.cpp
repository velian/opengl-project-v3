#include <GLM/ext.hpp>

#include "Utility/Gizmos.h"

#include "Scene/BoardPiece.h"
#include "Scene/BoardTile.h"
#include "Scene/Camera/Camera.h"

#include "Utility/Client.h"

#include <ImGui/imgui.h>

BoardPiece::~BoardPiece()
{
	
}

void BoardPiece::Update(double _deltaTime, Camera* _camera)
{
	if (m_enabled == false || m_parent == nullptr)
	{
		delete this;
	}

	m_index = m_parent->m_index;

	if (m_position.z == 0 && m_pieceColour == BLACK && !m_king)
	{
		m_king = true;
	}
	else if (m_position.z == 7 && m_pieceColour == RED && !m_king)
	{
		m_king = true;
	}
}

void BoardPiece::Initialize(glm::vec3 _position, float _radius, int _segments)
{
	m_position = _position;
	m_radius = _radius;
	m_segments = _segments;
	m_originPosition = m_position;
}

void BoardPiece::Draw(Camera* _camera)
{
	if (m_enabled != true) return;

	switch (m_pieceColour)
	{
	case PieceColour::RED:
	{
		if (m_king)
			Gizmos::addDisk(m_position, m_radius, m_segments, glm::vec4(1, 0.75, 0, 1));
		else
			Gizmos::addDisk(m_position, m_radius, m_segments, glm::vec4(1, 0, 0, 1));
	}break;
	case PieceColour::BLACK:
	{
		if(m_king)
			Gizmos::addDisk(m_position, m_radius, m_segments, glm::vec4(0.25, 0.5, 0.5, 1));
		else
			Gizmos::addDisk(m_position, m_radius, m_segments, glm::vec4(0.25, 0.25, 0.25, 1));
	}break;
	case PieceColour::NONE:
	{
		Gizmos::addDisk(m_position, m_radius, m_segments, glm::vec4(1, 1, 1, 1));
	}break;
	}
}