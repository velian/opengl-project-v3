#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "State/Examples/Example2.h"
#include "State/GameStateManager.h"

#include "Utility/Gizmos.h"
#include "Utility/Server.h"

Example2::Example2(unsigned int _id) : GameState(_id)
{
	m_network = new Server();

	m_serverIP = DEFAULT_IP;
	m_serverPort = DEFAULT_PORT;

	ImGui::PushConsoleCommand("/LISTUSERS", [&](char* command_line)
	{
		if (m_network->Initialized())
		{
			if (!m_network->m_clientList.empty())
			{
				ImGui::LogCustomConsole("\nUser List:");

				for each(char* client in m_network->m_clientList)
					ImGui::LogCustomConsole(client);
			}
			else
			{
				ImGui::LogCustomConsole("No clients on server.");
			}
		}
		else
		{
			ImGui::LogCustomConsole("Please initialize as client / server to list users.");
		}
	});
}

void Example2::Update(double _deltaTime)
{
	if (m_network->Initialized())
	{
		m_network->Update();
	}

	if (ImGui::GetConsoleUpdated())
	{
		if (m_network->Initialized())
			m_network->SendServerMessage(ImGui::GetConsoleBuffer());
	}

	if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE))
	{
		m_network->EndConnection();
	}
}

void Example2::Draw()
{
	DrawGUI("EXAMPLE 1");

	ImGui::ShowCustomConsole();
}

void Example2::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);

	if (ImGui::TreeNode("Network Options"))
	{
		if (ImGui::Button("Start Server"))
		{
			if (!m_network->Initialized())
			{
				m_network->InitializeServer(m_serverPort, (char*)m_serverIP.c_str());
			}
		}

		ImGui::InputText("Server IP", (char*)m_serverIP.c_str(), 64);
		ImGui::InputInt("Server Port", (int*)&m_serverPort);

		char* clientList[MAX_CLIENTS];

		for (int i = 0; i < m_network->m_clientList.size(); i++)
		{
			clientList[i] = m_network->m_clientList[i];
		}

		int currentNumber = 0;

		if (!m_network->m_clientList.empty())
			ImGui::ListBox((const char*)"Player List", &currentNumber, (const char**)clientList, (int)m_network->m_clientList.size());

		ImGui::TreePop();
	}
}