#include <ImGui\imgui.h>

#include <string>

#include "Scene/CheckerBoard.h"
#include "Scene/BoardTile.h"
#include "Scene/BoardPiece.h"
#include "Scene/Camera/Camera.h"

#include "Utility/SoundManager.h"
#include "Utility/Client.h"

CheckerBoard::CheckerBoard()
{
	Initialize();
}

void CheckerBoard::Initialize()
{
	m_boardPieces.clear();

	m_currentPlayer = 1;
	
	for each(BoardTile* tile in m_boardTiles)
	{
		tile = nullptr;
	}

	m_width = 8;
	m_height = 8;

	m_forceMovement = false;

	bool isWhite = true;
	for (int y = 0; y < m_height; y++)
	{
		for (int x = 0; x < m_width; x++)
		{
			BoardTile* newTile = new BoardTile((isWhite) ? BoardTile::WHITE : BoardTile::BLACK);
			newTile->m_boardColourOrigin = newTile->m_boardColour;
			newTile->m_position = glm::vec3(x, 0, y);
			newTile->m_scale = glm::vec3(0.5, 0.25, 0.5);
			newTile->m_parent = this;
			newTile->m_index.x = x;			
			newTile->m_index.y = y;

			if (newTile->m_boardColour == BoardTile::BLACK && y < 3)
			{
				BoardPiece* newPiece = new BoardPiece(0);
				newPiece->Initialize(newTile->m_position + glm::vec3(0, 0.26, 0), 0.5, 16);
				
				newPiece->m_index = newTile->m_index;

				newPiece->m_parent = newTile;
				newTile->m_child = newPiece;
				m_boardPieces.push_back(newPiece);
			}
			else if (newTile->m_boardColour == BoardTile::BLACK && y > 4)
			{
				BoardPiece* newPiece = new BoardPiece(1);
				newPiece->Initialize(newTile->m_position + glm::vec3(0, 0.26, 0), 0.5, 16);

				newPiece->m_index = newTile->m_index;

				newPiece->m_parent = newTile;
				newTile->m_child = newPiece;
				m_boardPieces.push_back(newPiece);
			}

			m_boardTiles[x][y] = newTile;
			isWhite = !isWhite;
		}
		isWhite = !isWhite;
	}
}

void CheckerBoard::PushSelected(BoardTile* _boardTile)
{
	m_selectedTiles.push_back(_boardTile);
}

void CheckerBoard::UpdateSelected(double _deltaTime, Client* _client)
{
	if (m_selectedTiles.size() >= 2)
	{
		if (m_selectedTiles[0] == m_selectedTiles[1])
		{
			CleanSelections();
			SoundManager::Instance()->PlayOneShot("back");
		}
		else if (m_selectedTiles[0]->m_child != nullptr && m_selectedTiles[1]->m_child == nullptr && m_selectedTiles[0]->m_neighbors.empty() == false && !m_selectedTiles[0]->m_child->m_lerpTimer >= 1)
		{
			if (m_selectedTiles[1]->m_boardColour == BoardTile::AVAILABLE)
			{
				m_forceMovement = false;
				_client->SendMoveOrder(m_selectedTiles[0]->m_index, m_selectedTiles[1]->m_index);
				CleanSelections();
				SoundManager::Instance()->PlayOneShot("validate");				
			}
			else if (m_selectedTiles[1]->m_child != nullptr)
			{
				CleanSelections();
				SoundManager::Instance()->PlayOneShot("back");
			}
		}		
	}
}

void CheckerBoard::CleanSelections(bool clearSelectedList)
{
	for each(BoardTile* selection in m_selectedTiles)
	{
		for each(BoardTile::MoveInfo* neighbor in selection->m_neighbors)
		{
			neighbor->endTile->m_boardColour = neighbor->endTile->m_boardColourOrigin;
		}

		selection->m_boardColour = selection->m_boardColourOrigin;
	}

	if (clearSelectedList)
	{
		m_selectedTiles.clear();
	}
}

void CheckerBoard::GenerateMoves(BoardTile* _boardTile)
{
	//for each neighbor, calculate if it is a valid move.
	//push it to a move list.

	_boardTile->m_neighbors.clear();
	_boardTile->GenerateNeighbors();

	if (_boardTile->m_neighbors.empty())
	{
		return;
	}

	for each(BoardTile::MoveInfo* neighbor in _boardTile->m_neighbors)
	{
		neighbor->endTile->m_boardColour = BoardTile::AVAILABLE;
	}
}

BoardTile* CheckerBoard::GetTileAtIndex(unsigned int _xIndex, unsigned int _yIndex)
{
	return m_boardTiles[_xIndex][_yIndex];
}

void CheckerBoard::RemovePieceAtIndex(glm::vec2 _index)
{
	if (m_boardTiles[(int)_index.x][(int)_index.y]->m_child == nullptr)
		return;
	else
	{
		m_boardTiles[(int)_index.x][(int)_index.y]->m_child->m_enabled = false;
		m_boardTiles[(int)_index.x][(int)_index.y]->m_child = nullptr;
	}
}

void CheckerBoard::Update(double _deltaTime, Camera* _camera, Client* _client)
{
	if (m_selectedTiles.empty() || m_selectedTiles[0]->m_neighbors.empty())
	{
		m_forceMovement = false;
	}

	if (m_forceMovement)
	{
		//m_selectedTiles[0]->Update(_deltaTime, _camera, _client);
		
		for each(BoardTile::MoveInfo* movement in m_selectedTiles[0]->m_neighbors)
		{		
			if (movement->endTile->m_boardColour == BoardTile::AVAILABLE)
			{
				movement->endTile->Update(_deltaTime, _camera, _client);
			}
		}
	}
	else
	{
		for (int x = 0; x < 8; x++)
		{
			for (int y = 0; y < 8; y++)
			{
				if (GetTileAtIndex(x, y) != nullptr)
					GetTileAtIndex(x, y)->Update(_deltaTime, _camera, _client);
			}
		}

		for (BoardPiece* piece : m_boardPieces)
		{
			if (piece != nullptr && piece->m_enabled == true)
				piece->Update(_deltaTime, _camera);
		}
	}	

	UpdateSelected(_deltaTime, _client);
}

void CheckerBoard::Draw(Camera* _camera)
{
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{
			if (GetTileAtIndex(x, y) != nullptr)
				GetTileAtIndex(x, y)->Draw(_camera);
		}
	}

	for (BoardPiece* piece : m_boardPieces)
	{
		if (piece != nullptr)
			piece->Draw(_camera);
	}
}