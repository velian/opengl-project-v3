NOTES:

Joystick - Joystick X Axis - axes[0]
Joystick - Joystick Y Axis - axes[1] (Negate to set to UP POSITIVE)
Joystick - Z Axis 	   - axes[2] (Negate to set to positive, correct values)
Joystick - Z Rotation      - axes[3] (Left is negative, right is positive)
Joystick - Slider          - axes[4] (Left is negative, right is positive)

Joystick - Button 1 	   - button[0] (Trigger, fire)
Joystick - Button 2 	   - button[1] (L1, missles)

Joystick - HAT UP	   - button[12]
Joystick - HAT RIGHT 	   - button[13]
Joystick - HAT DOWN	   - button[14]
Joystick - HAT LEFT	   - button[15]