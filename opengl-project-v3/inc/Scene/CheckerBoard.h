#pragma once

#include <vector>
#include <GLM\glm.hpp>

class Camera;
class BoardTile;
class BoardPiece;
class Client;
class CheckerBoard
{
public:

	CheckerBoard();
	~CheckerBoard();

	void Update(double _deltaTime, Camera* _camera, Client* _client);
	void Draw(Camera* _camera);

	void GenerateMoves(BoardTile* _boardPiece);
	void PushSelected(BoardTile* _boardTile);

	void RemovePieceAtIndex(glm::vec2 _index);

	BoardTile* GetTileAtIndex(unsigned int _xIndex, unsigned int _yIndex);
	BoardTile* GetTileAtIndex(glm::vec2 _index){ return GetTileAtIndex(_index.x, _index.y); }

	void UpdateSelected(double _deltaTime, Client* _client);
	void CleanSelections(bool clearSelectedList = true);

	std::vector<BoardTile*> m_selectedTiles;

	int m_width, m_height;	//board width and height

	BoardTile* m_boardTiles[8][8];
	std::vector<BoardPiece*> m_boardPieces;

	bool m_forceMovement;

	unsigned int m_currentPlayer;

private:

	void Initialize();
	
};
