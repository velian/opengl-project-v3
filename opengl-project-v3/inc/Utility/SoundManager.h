#pragma once

#include <vector>

#include "FMOD\fmod.hpp"
#include "Utility\Sound.h"

class SoundManager
{
public:
	
	static SoundManager* Instance();

	void Update();

	void PlayOneShot(char* _soundName, bool _loop = false);
	void StopSound(char* _soundName);

	void LoadSound(char* _filePath, char* _soundName);
	void AddSound(Sound* _sound);

	FMOD::System* m_system;
	std::vector<Sound*> m_soundList;

private:	

	SoundManager();
	SoundManager(const SoundManager*&) = delete;

	static SoundManager* m_instance;
};