#ifndef OVERLAY_H
#define OVERLAY_H

#include "Utility/RenderTarget.h"

class Texture;
class Overlay : public RenderTarget
{
public:

	Overlay(std::string _texturePath);
	Overlay(Texture* _texture);

	virtual void Update();
	virtual void Draw(Camera* _camera);

private:

	Texture* m_texture;

};

#endif OVERLAY_H