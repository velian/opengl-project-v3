#include "State/GameStateManager.h"
#include "State/GameState.h"

GameState::GameState(unsigned int _id)
{
	m_id = _id;
}

void GameState::Update(double _deltaTime)
{

}

void GameState::Draw()
{

}

void GameState::DrawGUI(char* _stateName)
{
	if (ImGui::CollapsingHeader("State Settings"))
	{
		ImGui::SameLine();
		ImGui::Text("CURRENT STATE : %s", _stateName);

		if (ImGui::Button("PREVIOUS STATE")){
			GameStateManager::GetInstance()->PreviousState();
		}
		ImGui::SameLine();
		if (ImGui::Button("NEXT STATE")){
			GameStateManager::GetInstance()->NextState();
		}
	}
	ImGui::Separator();
}