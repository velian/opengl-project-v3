#include "Scene/Renderable/RTOverlay.h"
#include "Utility/Texture.h"
#include "Utility/ShaderLoader.h"

RTOverlay::RTOverlay()
{
	Create();
}

void RTOverlay::Update()
{

}

void RTOverlay::Draw(Camera* _camera)
{
	if (m_programID == NULL)
	{
		printf("Tried to render RTOverlay with null program ID\nSetting to default program\n");

		//Create default shaders in case we screw up
		std::vector<std::string> defaultShaders;
		defaultShaders.push_back("./shaders/renderTarget/renderTarget.vs");
		defaultShaders.push_back("./shaders/renderTarget/renderTarget.fs");

		m_programID = ShaderLoader::GetInstance()->LoadShaders(defaultShaders);
	}

	glUseProgram(m_programID);

	if (m_FBOTexture != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_FBOTexture);
		glUniform1i(glGetUniformLocation(m_programID, "diffuse"), 0);
	}

	glBindVertexArray(m_shaderData->m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
	glUseProgram(0);
}