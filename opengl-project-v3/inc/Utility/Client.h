#pragma once

#include <vector>
#include "Utility/Network.h"

using namespace RakNet;
class BoardTile;
class Client : public Network
{
public:

	Client();
	~Client();

	void InitializeClient();
	void InitializeClient(char* _clientName);
	void InitializeClient(char* _clientName, unsigned int _port, char* _ipAddress);

	virtual void Update(double _deltaTime, CheckerBoard* m_checkerboard);

	void SendClientMessage(char* _serverMessage);
	void SendMoveOrder(glm::vec2 _startIndex, glm::vec2 _endIndex);
	void SendRemoveOrder(glm::vec2 _index);
	void SetCurrentPlayer(unsigned int _playerNumber);

	unsigned int m_id;

private:

	void UpdateServer();	
};