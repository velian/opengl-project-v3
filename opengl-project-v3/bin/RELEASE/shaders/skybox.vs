#version 410

layout (location = 0) in vec3 Position;

out vec3 TexCoords;

uniform mat4 projectionView;

void main()
{
    gl_Position = projectionView * vec4(Position, 1);
    TexCoords = Position;
}