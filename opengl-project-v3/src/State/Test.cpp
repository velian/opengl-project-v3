#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <ImGui/imgui.h>

#include <iostream>

#include "State/Test.h"
#include "State/GameStateManager.h"
#include "Scene/Camera/Camera.h"

#include "Scene/Renderable/Object.h"
#include "Scene/Renderable/RTOverlay.h"
#include "Scene/Renderable/Overlay.h"
#include "Scene/Renderable/Skybox.h"
#include "Scene/Renderable/GPUParticleEmitter.h"
#include "Scene/Light.h"

#include "Utility/ShaderLoader.h"

Test::Test(unsigned int _id) : GameState(_id)
{
	m_camera = new Camera(3.14159f * 0.25f, 4.0f / 3.0f, 0.01, 100);
	m_camera->setLookAtFrom(glm::vec3(5), glm::vec3(0, 0, 0));

	std::vector<char*> playerTextures;
	playerTextures.push_back("./objects/ARC170/ARC170.tga");

	m_player = new Object("./objects/ARC170/ARC170.obj", playerTextures);
	m_player->Initialize();

	m_player->m_rotationSpeed = 0.1;

	m_player->m_transform = glm::translate(glm::vec3(2, 0, 2));
	m_player->m_transform = m_player->m_transform * glm::scale(glm::vec3(0.05, 0.05, 0.05));

	std::vector<std::string> objectShaders;
	objectShaders.push_back("./shaders/object/objectPerlin.vs");
	objectShaders.push_back("./shaders/object/objectPerlin.fs");
	objectShaders.push_back("./shaders/object/objectPerlin.gs");
	unsigned int perlinShader = ShaderLoader::GetInstance()->LoadShaders(objectShaders);

	std::vector<char*> texList;
	texList.push_back("./images/rockLayer1.jpg");
	texList.push_back("./images/rockLayer2.jpg");
	texList.push_back("./images/rockLayer3.jpg");

	m_object = new Object("./objects/static/sphere.obj", texList);
	m_object->Initialize(perlinShader);

	m_objects = new Object[10];


	m_visualizeAllLayers = false;

	for (int i = 2; i < 12; i++)
	{
		float scale = glm::linearRand(0.08f, 0.2f);
		m_objects[i - 2] = Object(*m_object);
		m_objects[i - 2].m_transform *= glm::scale(glm::vec3(scale, scale, scale));
		m_objects[i - 2].m_transform *= glm::translate(glm::vec3(i * glm::linearRand(1000, 1200), sin(i), i * glm::linearRand(1000, 1200)));
		m_objects[i - 2].InitializePerlin(i, 3, glm::linearRand(3.f, 5.f), glm::linearRand(3.f, 5.f), glm::linearRand(3.f, 5.f));
		m_objects[i - 2].m_rotationSpeed = glm::linearRand(0.1f, 0.2f);
		srand(i);
	}

	m_rtOverlay = new RTOverlay();
	m_overlay = new Overlay("./images/cockpit.png");

	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back("./images/skybox/right.png");
	skyboxPaths.push_back("./images/skybox/left.png");
	skyboxPaths.push_back("./images/skybox/top.png");
	skyboxPaths.push_back("./images/skybox/bottom.png");
	skyboxPaths.push_back("./images/skybox/front.png");
	skyboxPaths.push_back("./images/skybox/back.png");

	m_skybox = new Skybox(skyboxPaths);
	m_emitter = new GPUParticleEmitter(glm::vec3(0, 0, 0), new Texture("./images/smoke.png", 200, 200, 0.25), 0.1f, 0.2f);
	m_light = new Light(glm::vec3(2, 2, 2));
}

void Test::Update(double _deltaTime)
{
	m_camera->update(_deltaTime);
	m_object->Update(_deltaTime);
	m_emitter->Update(_deltaTime);
	
	glm::vec3 origin(0, 0, 0);

	for (int i = 0; i < 10; i++)
	{				
		glm::vec3 current(m_objects[i].m_transform[3].xyz);
		float distanceFromOrigin = glm::distance(origin, current);

		m_objects[i].m_transform[3].xyz = glm::vec3(sinf((float)glfwGetTime() * m_objects[i].m_rotationSpeed) * distanceFromOrigin, sin(i), cosf((float)glfwGetTime() * m_objects[i].m_rotationSpeed) * distanceFromOrigin);
		m_objects[i].Update(_deltaTime);
	}

	glm::vec3 current(m_player->m_transform[3].xyz);
	float distanceFromOrigin = glm::distance(origin, current);
	m_player->m_transform[3].xyz = glm::vec3(cosf((float)glfwGetTime() / 6.5) * distanceFromOrigin, 0, sinf((float)glfwGetTime() / 6.5) * distanceFromOrigin);
	//m_player->Update(_deltaTime);
}

void Test::Draw()
{
	DrawGUI("TEST STATE");

	if (ImGui::Checkbox("Visualize All Layers", &m_visualizeAllLayers))
	{
		m_object->m_layerVisualizer = m_visualizeAllLayers;

		for (int i = 0; i < 10; i++)
		{
			m_objects[i].m_layerVisualizer = m_visualizeAllLayers;
		}
	}

	m_light->DrawGUI("Scene Light");
	m_emitter->DrawGUI("Smoke Particles 1");
	m_object->DrawGUI("Main Asteroid");
	
	for (int i = 0; i < 10; i++)
	{
		char nameBuffer[32];
		sprintf(nameBuffer, "Asteroid %i", i);
		m_objects[i].DrawGUI(nameBuffer);
	}

	m_rtOverlay->SetActive();
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_skybox->Draw(m_camera);
	m_object->Draw(m_camera, m_light);
	m_player->Draw(m_camera, m_light);

	for (int i = 0; i < 10; i++)
	{
		m_objects[i].Draw(m_camera, m_light);
	}

	m_emitter->Draw(m_camera);
	
	//m_overlay->Draw(m_camera);

	m_rtOverlay->ClearActive();
	m_rtOverlay->Draw(m_camera);
}