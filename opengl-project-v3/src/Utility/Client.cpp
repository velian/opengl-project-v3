#include "Scene/BoardTile.h"

#include "Utility/Client.h"

#include <string>

Client::Client()
{

}

void Client::InitializeClient()
{
	InitializeClient(DEFAULT_NAME, DEFAULT_PORT, DEFAULT_IP);
}

void Client::InitializeClient(char* _clientName)
{
	InitializeClient(_clientName, DEFAULT_PORT, DEFAULT_IP);
}

void Client::InitializeClient(char* _clientName, unsigned int _port, char* _ipAddress)
{
	SocketDescriptor sd;
	m_peer->Startup(1, &sd, 1);
	m_isServer = false;
	m_peer->Connect(_ipAddress, _port, 0, 0);

	m_clientName = _clientName;

	m_id = NULL;

	m_initialized = true;
	m_running = true;
}

void Client::SendClientMessage(char* _message)
{
	BitStream bs;
	bs.Write((RakNet::MessageID)ID_CLIENT_MESSAGE);

	char buffer[256];
	sprintf(buffer, "%s: %s", m_clientName, _message);

	bs.Write(buffer);
	m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_systemAddress, false);
}

void Client::Update(double _deltaTime, CheckerBoard* _checkerboard)
{
	if (m_running)
	{
		for (m_packet = m_peer->Receive(); m_packet; m_peer->DeallocatePacket(m_packet), m_packet = m_peer->Receive())
		{
			switch (m_packet->data[0])
			{
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				ImGui::LogCustomConsole("Our connection request has been accepted.\n");					
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_CLIENT_CONNECT);
				bsOut.Write(m_clientName);
				m_peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_packet->systemAddress, false);
				m_systemAddress = m_packet->systemAddress;
			} break;

			case ID_NO_FREE_INCOMING_CONNECTIONS:
			{
				ImGui::LogCustomConsole("The server is full.\n");
			} break;

			case ID_SERVER_BROADCAST:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				if (rs.Find(m_clientName) == std::string::npos)
					ImGui::LogCustomConsole((char*)rs.C_String());
			} break;

			case ID_SERVER_UPDATE_CLIENT_LIST:
			{
				int clientListSize = 0;
				std::vector<char*> clientList;
				RakNet::BitStream bs(m_packet->data, m_packet->length, false);
				bs.IgnoreBytes(sizeof(RakNet::MessageID));
				bs.Read(clientListSize);

				if (m_id == NULL)
				{
					m_id = clientListSize;

					std::string msg = "Assigned an ID of : " + std::to_string(m_id);
					std::string colour = (m_id == 1) ? "Your colour is RED. You will move first." : "Your colour is BLACK. You will move second.";

					ImGui::LogCustomConsole((char*)msg.c_str());
					ImGui::LogCustomConsole((char*)colour.c_str());
				}

				for (int i = 0; i < clientListSize; i++)
				{
					int size;
					bs.Read(size);

					char* name = new char[size];
					bs.Read(name);
					clientList.push_back(name);
				}				

				m_clientList = clientList;
			} break;

			case ID_CLIENT_MOVE_PIECE:
			{
				glm::vec2 startIndex;
				glm::vec2 endIndex;

				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(startIndex);
				bsIn.Read(endIndex);

				_checkerboard->GetTileAtIndex(startIndex)->GenerateNeighbors();
				_checkerboard->GetTileAtIndex(startIndex)->MovePiece(_deltaTime, _checkerboard->GetTileAtIndex(endIndex), this);
			} break;

			case ID_CLIENT_REMOVE_PIECE:
			{
				glm::vec2 index;

				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(index);

				_checkerboard->RemovePieceAtIndex(index);
			} break;

			case ID_CLIENT_SET_CURRENT_PLAYER:
			{
				unsigned int playerNumber;

				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(playerNumber);

				if (playerNumber == m_id)
				{
					ImGui::LogCustomConsole("It is your turn to move.");
				}
				else
				{
					ImGui::LogCustomConsole("It is the opponent's turn.");
				}				

				_checkerboard->m_currentPlayer = playerNumber;

			} break;

			default: break;
			}
		}
	}
}

void Client::SendMoveOrder(glm::vec2 _startIndex, glm::vec2 _endIndex)
{
	RakNet::BitStream clientUpdate;
	clientUpdate.Write((RakNet::MessageID)ID_CLIENT_SEND_MOVE_ORDER);

	clientUpdate.Write(_startIndex);
	clientUpdate.Write(_endIndex);

	m_peer->Send(&clientUpdate, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_systemAddress, false);
}

void Client::SendRemoveOrder(glm::vec2 _index)
{
	RakNet::BitStream clientUpdate;
	clientUpdate.Write((RakNet::MessageID)ID_CLIENT_SEND_REMOVE_ORDER);

	clientUpdate.Write(_index);

	m_peer->Send(&clientUpdate, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_systemAddress, false);
}

void Client::SetCurrentPlayer(unsigned int _playerNumber)
{
	RakNet::BitStream clientUpdate;
	clientUpdate.Write((RakNet::MessageID)ID_CLIENT_SET_CURRENT_PLAYER);

	clientUpdate.Write(_playerNumber);

	m_peer->Send(&clientUpdate, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_systemAddress, false);
}