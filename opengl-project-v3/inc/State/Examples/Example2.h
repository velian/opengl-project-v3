#ifndef EXAMPLE2_H
#define EXAMPLE2_H

#include "State/GameState.h"

class Server;
class Example2 : public GameState
{
public:

	Example2(unsigned int _id);

	virtual void Update(double _deltaTime);
	virtual void Draw();

	virtual void DrawGUI(char* _stateName);

private:
	//GameState Variables
	Server* m_network;
	std::string m_serverIP;
	unsigned int m_serverPort;

	bool m_serverRunning;
	//
};

#endif EXAMPLE2_H