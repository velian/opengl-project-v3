#include <GLFW/glfw3.h>
#include <GL/gl_core_4_4.h>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include <stdio.h>

#include "Input/JoystickManager.h"

JoystickManager* JoystickManager::m_instance = nullptr;

JoystickManager* JoystickManager::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new JoystickManager();
	}
	return m_instance;
}

bool JoystickManager::ButtonPressed(unsigned int _joystickButton, unsigned int _joystickNumber, bool _pressOnce)
{
	m_joystickButtons = glfwGetJoystickButtons(_joystickNumber, &m_buttonCount);

	if (m_buttonCount != 0)
	{
		if (m_joystickButtons[_joystickButton] == GLFW_PRESS)
		{
			return true;
		}
	}

	return false;
}

float JoystickManager::GetXAxis(unsigned int _joystickNumber)
{
	m_joystickAxes = glfwGetJoystickAxes(_joystickNumber, &m_axesCount);

	return m_joystickAxes[0];
}

float JoystickManager::GetYAxis(unsigned int _joystickNumber)
{
	m_joystickAxes = glfwGetJoystickAxes(_joystickNumber, &m_axesCount);

	return m_joystickAxes[1];
}

float JoystickManager::GetZAxis(unsigned int _joystickNumber)
{
	m_joystickAxes = glfwGetJoystickAxes(_joystickNumber, &m_axesCount);

	return m_joystickAxes[2];
}

float JoystickManager::GetZAxisRotation(unsigned int _joystickNumber)
{
	m_joystickAxes = glfwGetJoystickAxes(_joystickNumber, &m_axesCount);

	return m_joystickAxes[3];
}

float JoystickManager::GetSlider(unsigned int _joystickNumber)
{
	m_joystickAxes = glfwGetJoystickAxes(_joystickNumber, &m_axesCount);

	return m_joystickAxes[4];
}

JoystickManager::~JoystickManager()
{

}