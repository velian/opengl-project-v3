#ifndef EXAMPLE1_H
#define EXAMPLE1_H

#include "State/GameState.h"

class Camera;
class Client;
class CheckerBoard;
class Example1 : public GameState
{
public:

	Example1(unsigned int _id);

	virtual void Update(double _deltaTime);
	virtual void Draw();

	virtual void DrawGUI(char* _stateName);

private:
	//GameState Variables
	Camera* m_camera;
	CheckerBoard* m_checkerBoard;
	Client* m_network;
	std::string m_clientName;
	std::string m_serverIP;
	unsigned int m_serverPort;

	bool m_serverRunning;
	//
};

#endif EXAMPLE1_H