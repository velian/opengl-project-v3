#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>

#include <IMGUI/imgui.h>

#include <GLM/glm.hpp>
#include <GLM/ext.hpp>

#include "Scene/Renderable/GPUParticleEmitter.h"
#include "Scene/Camera/Camera.h"

GPUParticleEmitter::GPUParticleEmitter(vec3 _position, Texture* _texture, float _startSize, float _endSize)
{
	Initialize(_position, _texture, _startSize, _endSize);
}

void GPUParticleEmitter::Initialize(vec3 _position, Texture* _texture, float _startSize, float _endSize)
{
	m_position = _position;
	m_startColour = glm::vec4(1, 1, 1, 1);
	m_endColour = glm::vec4(1, 1, 1, 1);
	m_startSize = _startSize;
	m_endSize = _endSize;
	m_emitRate = 0.25;
	m_lifeSpanMin = 1;
	m_lifeSpanMax = 2;
	m_maxParticles = 200;
	m_velocityMin = 0;
	m_velocityMax = 0;
	m_texture = _texture;

	std::vector<std::string> updateShaders;
	std::vector<std::string> drawShaders;

	updateShaders.push_back("./shaders/particle/gpuparticleUpdate.vs");
	drawShaders.push_back("./shaders/particle/gpuparticle.vs");
	drawShaders.push_back("./shaders/particle/gpuparticle.fs");
	drawShaders.push_back("./shaders/particle/gpuparticle.gs");

	char* varyings[] = 
	{
		"vPosition",
		"vVelocity",
		"vLifetime",
		"vLifespan"
	};

	m_updateShader = ShaderLoader::GetInstance()->LoadShaders(updateShaders, varyings);
	m_drawShader = ShaderLoader::GetInstance()->LoadShaders(drawShaders);

	m_particles = new GPUParticle[m_maxParticles];

	m_activeBuffer = 0;

	Create();
	CreateDrawShader();
	CreateUpdateShader();
}

void GPUParticleEmitter::Create()
{
	m_shaderData = new ShaderData[2];

	//Generate
	glGenVertexArrays(1, &(m_shaderData[0].m_VAO));
	glGenBuffers(1, &(m_shaderData[0].m_VBO));
	glGenVertexArrays(1, &(m_shaderData[1].m_VAO));
	glGenBuffers(1, &(m_shaderData[1].m_VBO));

	//Setup The Buffers
	glBindVertexArray(m_shaderData[0].m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[0].m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), m_particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0); //Position
	glEnableVertexAttribArray(1); //Velocity
	glEnableVertexAttribArray(2); //Lifetime
	glEnableVertexAttribArray(3); //Lifespan

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	glBindVertexArray(m_shaderData[1].m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[1].m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), 0, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	glBindVertexArray(0);
}

void GPUParticleEmitter::CreateDrawShader()
{
	glUseProgram(m_drawShader);

	//Bind size info
	int location = glGetUniformLocation(m_drawShader, "StartSize");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader, "EndSize");
	glUniform1f(location, m_endSize);

	//Bind colour info
	location = glGetUniformLocation(m_drawShader, "StartColour");
	glUniform4fv(location, 1, glm::value_ptr(m_startColour));
	location = glGetUniformLocation(m_drawShader, "EndColour");
	glUniform4fv(location, 1, glm::value_ptr(m_endColour));

	glUseProgram(0);
}

void GPUParticleEmitter::CreateUpdateShader()
{
	glUseProgram(m_updateShader);

	int location = glGetUniformLocation(m_updateShader, "lifeMin");
	glUniform1f(location, m_lifeSpanMin);
	location = glGetUniformLocation(m_updateShader, "lifeMax");
	glUniform1f(location, m_lifeSpanMax);

	glUseProgram(0);
}

void GPUParticleEmitter::Update(double _deltaTime)
{
	m_texture->Update(_deltaTime);

	glUseProgram(m_updateShader);

	int location = glGetUniformLocation(m_updateShader, "time");
	glUniform1f(location, (float)glfwGetTime());

	location = glGetUniformLocation(m_updateShader, "deltaTime");
	glUniform1f(location, (GLfloat)_deltaTime);

	location = glGetUniformLocation(m_updateShader, "emitterPosition");
	glUniform3fv(location, 1, &m_position[0]);

	location = glGetUniformLocation(m_updateShader, "lifeMin");
	glUniform1f(location, m_lifeSpanMin);

	location = glGetUniformLocation(m_updateShader, "lifeMax");
	glUniform1f(location, m_lifeSpanMax);

	glEnable(GL_RASTERIZER_DISCARD);

	glBindVertexArray(m_shaderData[m_activeBuffer].m_VAO);

	unsigned int secondaryBuffer = (m_activeBuffer + 1) % 2;

	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_shaderData[secondaryBuffer].m_VBO);
	glBeginTransformFeedback(GL_POINTS);

	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	glUseProgram(0);
}

void GPUParticleEmitter::Draw(Camera* _camera)
{
	glUseProgram(m_drawShader);

	unsigned int secondaryBuffer = (m_activeBuffer + 1) % 2;

	int location = glGetUniformLocation(m_drawShader, "ProjectionView");
	glUniformMatrix4fv(location, 1, false, &_camera->getProjectionView()[0][0]);

	location = glGetUniformLocation(m_drawShader, "CameraTransform");
	glUniformMatrix4fv(location, 1, false, &_camera->getTransform()[0][0]);

	location = glGetUniformLocation(m_drawShader, "StartSize");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader, "EndSize");
	glUniform1f(location, m_endSize);

	//Bind colour info
	location = glGetUniformLocation(m_drawShader, "StartColour");
	glUniform4fv(location, 1, glm::value_ptr(m_startColour));

	location = glGetUniformLocation(m_drawShader, "EndColour");
	glUniform4fv(location, 1, glm::value_ptr(m_endColour));

	if (m_texture != nullptr)
	{
		float frameWidthU = (float)m_texture->m_frameWidth / (float)m_texture->m_imageWidth;
		float frameHeightV = (float)m_texture->m_frameHeight / (float)m_texture->m_imageHeight;

		location = glGetUniformLocation(m_drawShader, "CurrentFrame");
		glUniform1i(location, m_texture->m_currentFrame);

		location = glGetUniformLocation(m_drawShader, "FrameWidth");
		glUniform1f(location, frameWidthU);

		location = glGetUniformLocation(m_drawShader, "FrameHeight");
		glUniform1f(location, frameHeightV);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture->GetID());

		location = glGetUniformLocation(m_drawShader, "tex");
		glUniform1i(location, 0);
	}

	glDisable(GL_DEPTH_TEST);

	glBindVertexArray(m_shaderData[secondaryBuffer].m_VAO);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);
	glBindVertexArray(0);

	m_activeBuffer = secondaryBuffer;
	glEnable(GL_DEPTH_TEST);

	glUseProgram(0);
}

void GPUParticleEmitter::DrawGUI(char* _emitterName)
{
	if (ImGui::TreeNode(_emitterName))
	{
		float startColour[4] = { m_startColour.r, m_startColour.g, m_startColour.b, m_startColour.a };
		float endColour[4] = { m_endColour.r, m_endColour.g, m_endColour.b, m_endColour.a };

		ImGui::ColorEdit4("Start Colour", startColour);
		ImGui::ColorEdit4("End Colour", endColour);
		ImGui::InputFloat("Lifespan - Minimum", &m_lifeSpanMin, 1.f, 10.f);
		ImGui::InputFloat("Lifespan - Maximum", &m_lifeSpanMax, 1.f, 10.f);
		ImGui::InputFloat("Size - Start", &m_startSize, 0.1f, 1.f);
		ImGui::InputFloat("Size - End", &m_endSize, 0.1f, 1.f);

		m_startColour.xyzw = glm::vec4(startColour[0], startColour[1], startColour[2], startColour[3]);
		m_endColour.xyzw = glm::vec4(endColour[0], endColour[1], endColour[2], endColour[3]);

		ImGui::TreePop();
	}
}