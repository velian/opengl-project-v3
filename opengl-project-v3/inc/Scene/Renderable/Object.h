#ifndef OBJECT_H
#define OBJECT_H

#include <GLM/glm.hpp>

#include <vector>

#include "Utility/ShaderLoader.h"
#include "Utility/ObjectData.h"

struct Light;
class Camera;
class Texture;
class Object
{
public:

	Object(){}
	Object(char* _filePath, std::vector<char*> _texturePaths);

	void Initialize(unsigned int _programID = 0);
	void InitializePerlin(int _seed, int _octaves, float _scale, float _amplitude, float _persistance);

	void Update(double _deltaTime);
	void Draw(Camera* _camera, Light* _light = nullptr);
	void DrawGUI(char* _objectName);

	unsigned int m_programID;

	glm::mat4 m_transform;

	// Debug Rotation
	float m_rotationSpeed;

	//Debug Layer Visualizer
	bool m_layerVisualizer;

private:

	void Create(unsigned int _programID);

	ObjectData* m_data;
	ShaderData* m_shaderData;
	std::vector<Texture*> m_textures;

	unsigned int m_perlinTexture;
	

	int m_perlinSeed;
	int m_octaves;
	float m_scale;
	float m_amplitude;
	float m_persistance;	
};


#endif