#ifndef JOYSTICKMANAGER_H
#define JOYSTICKMANAGER_H

#include <GLM/glm.hpp>

class JoystickManager
{
public:

	bool ButtonPressed		(unsigned int _joystickButton, unsigned int _joystickNumber = 0, bool _pressOnce = true);

	float GetXAxis			(unsigned int _joystickNumber = 0);
	float GetYAxis			(unsigned int _joystickNumber = 0);
	float GetZAxis			(unsigned int _joystickNumber = 0);
	float GetZAxisRotation	(unsigned int _joystickNumber = 0);
	float GetSlider			(unsigned int _joystickNumber = 0);

	static JoystickManager* GetInstance();

private:

	JoystickManager(){};
	JoystickManager(JoystickManager const&) = delete;
	~JoystickManager();

	static JoystickManager* m_instance;
	
	int m_buttonCount;
	int m_axesCount;
	const unsigned char* m_joystickButtons;
	const float* m_joystickAxes;
};

#endif APPLICATION_H