#include <iostream>

#include <IMGUI\imgui.h>
#include <Utility\Gizmos.h>
#include <GLFW\glfw3.h>

#include "Scene/Camera/Camera.h"

#include "Scene/BoardTile.h"
#include "Scene/BoardPiece.h"
#include "Scene/Checkerboard.h"

#include "Utility/SoundManager.h"

#include "Utility/Client.h"

BoardTile::BoardTile()
{
	m_boardColour = BoardTile::BoardColour::NONE;
	m_boardColourOrigin = m_boardColour;
	m_child = nullptr;
	m_parent = nullptr;
	m_index = glm::vec2(-1,-1);
}

void BoardTile::GenerateNeighbors()
{
	if (m_child == nullptr) return;

	m_neighbors.clear();

	//Generate neighbors
	for (int i = 0; i < 4; i++)
	{
		glm::vec2 direction;
		MoveInfo* moveInfo = new MoveInfo(nullptr, nullptr);

		switch (i)
		{
		case 0: //NORTH EAST
		{
			if (m_child->m_king || m_child->m_pieceColour == BoardPiece::RED)
			{
				moveInfo->moveDirection = NorthEast;
				direction = NE;
			}
		}break;
		case 1: //NORTH WEST
		{
			if (m_child->m_king || m_child->m_pieceColour == BoardPiece::RED)
			{
				moveInfo->moveDirection = NorthWest;
				direction = NW;
			}
		}break;
		case 2: //SOUTH EAST
		{
			if (m_child->m_king || m_child->m_pieceColour == BoardPiece::BLACK)
			{
				moveInfo->moveDirection = SouthEast;
				direction = SE;
			}
		}break;
		case 3: //SOUTH WEST
		{
			if (m_child->m_king || m_child->m_pieceColour == BoardPiece::BLACK)
			{
				moveInfo->moveDirection = SouthWest;
				direction = SW;
			}
		}break;
		}

		if (m_index.x + direction.x > m_parent->m_width  - 1 || m_index.x + direction.x < 0)
			continue;
		if (m_index.y + direction.y > m_parent->m_height - 1 || m_index.y + direction.y < 0)
			continue;
		
		//If condition, push neighbor
		BoardTile* pTile = m_parent->GetTileAtIndex(m_index + direction);

		bool bTileIsClear = pTile->m_child == nullptr;

		if (bTileIsClear)
		{
			moveInfo->endTile = pTile;
			m_neighbors.push_back(moveInfo);
			continue;
		}

		if (m_index.x + (direction.x + direction.x) > m_parent->m_width - 1|| m_index.x + (direction.x + direction.x) < 0)
			continue;
		if (m_index.y + (direction.y + direction.y) > m_parent->m_height - 1 || m_index.y + (direction.y + direction.y) < 0)
			continue;

		bool bTileHasEnemyPiece = pTile->m_child->m_pieceColour != m_child->m_pieceColour;

		if (!bTileHasEnemyPiece) continue;

		bool bJumpPossible = m_parent->GetTileAtIndex(m_index + direction + direction)->m_child == nullptr;

		if (bJumpPossible)
		{
			moveInfo->endTile = m_parent->GetTileAtIndex(m_index + direction + direction);
			moveInfo->jumpTile = m_parent->GetTileAtIndex(m_index + direction);
			m_neighbors.push_back(moveInfo);
		}
	}
}

void BoardTile::Update(double _deltaTime, Camera* _camera, Client* _client)
{
	if (m_parent->m_currentPlayer != _client->m_id)
		return;

	double mouseX, mouseY;
	glfwGetCursorPos(glfwGetCurrentContext(),  &mouseX, &mouseY);	

	glm::vec3 raycastPos = _camera->pickAgainstPlane(mouseX, mouseY, glm::vec4(0, 1, 0, 0));

	if (raycastPos.x < m_position.x + 0.5f && raycastPos.x > m_position.x - 0.5f &&
		raycastPos.z < m_position.z + 0.5f && raycastPos.z > m_position.z - 0.5f)
	{
		if (ImGui::IsMouseClicked(0))
		{
			if (m_parent != nullptr)
			{
				if (m_parent->m_forceMovement)
				{
					m_parent->PushSelected(this);
					return;
				}

				if (m_parent->m_selectedTiles.empty() && m_child == nullptr) return;

				if (m_parent->m_selectedTiles.size() < 1 && _client->m_id - 1 != m_child->m_pieceColour ) return;

				m_parent->PushSelected(this);
				
				if (m_parent->m_selectedTiles.size() != 2)
				{
					m_parent->GenerateMoves(this);
				}

				if (m_parent->m_selectedTiles.size() == 1)
				{
					m_boardColour = BoardColour::SELECTED;

					SoundManager::Instance()->PlayOneShot("select");
				}
				else if (m_parent->m_selectedTiles.size() > 1)
				{
					if (m_parent->m_selectedTiles[0] == this || m_boardColourOrigin == WHITE || m_parent->m_selectedTiles[1]->m_boardColour != AVAILABLE && !m_parent->m_forceMovement)
					{
						SoundManager::Instance()->PlayOneShot("back");
						m_parent->CleanSelections(true);
						return;
					}
				}
			}
		}
	}
}

void BoardTile::MovePiece(double _deltaTime, BoardTile* _boardTile, Client* _client)
{
	if (m_child != nullptr)
	{
		//m_child->Lerp(_deltaTime, _boardTile, _client);

		glm::vec3 offset(0, 0.26, 0);

		m_child->m_position = _boardTile->m_position + offset;
		m_child->m_index = _boardTile->m_index;

		m_child->m_parent = _boardTile;
		m_child->m_originPosition = m_position;
		_boardTile->m_child = m_child;

		m_child = nullptr;

		m_boardColour = m_boardColourOrigin;
		_boardTile->m_boardColour = _boardTile->m_boardColourOrigin;

		_boardTile->m_neighbors.clear();

		m_parent->CleanSelections();

		bool multi = false;
		bool anyAvailable = false;

		//For each possible movement direction, calculate if we just jumped a tile
		for each (BoardTile::MoveInfo* movement in m_neighbors)
		{
			if (movement->jumpTile != nullptr)
			{
				if (movement->endTile == _boardTile)
				{					
					_client->SendRemoveOrder(movement->jumpTile->m_index);
					m_neighbors.clear();
					multi = true;

					break;
				}
			}
		}

		if (multi)
		{
			m_parent->GenerateMoves(_boardTile);

			m_parent->m_forceMovement = false;

			for each(BoardTile::MoveInfo* movement in _boardTile->m_neighbors)
			{
				if (movement->jumpTile != nullptr)
				{
					if (movement->jumpTile->m_child != nullptr)
						m_parent->m_forceMovement = true;					
				}
			}
		}

		if (m_parent->m_forceMovement)
		{
			m_parent->GenerateMoves(_boardTile);
			_boardTile->GenerateNeighbors();

			if (_boardTile->m_neighbors.empty())
			{
				m_parent->m_forceMovement = false;
			}

			for each(BoardTile::MoveInfo* movement in _boardTile->m_neighbors)
			{
				if (movement->endTile == this)
				{
					movement->endTile->m_boardColour = BoardColour::BLACK;
				}

				if (movement->jumpTile == nullptr)
				{
					movement->endTile->m_boardColour = BoardColour::BLACK;
				}

				if (movement->jumpTile != nullptr)
				{
					if (movement->jumpTile->m_child == nullptr)
					{
						movement->endTile->m_boardColour = BoardColour::BLACK;
					}
				}			
			}		

			

			for (int i = 0; i < _boardTile->m_neighbors.size(); i++)
			{
				if (_boardTile->m_neighbors[i]->endTile->m_boardColour == BoardColour::AVAILABLE)
				{
					anyAvailable = true;
				}
			}

			if (m_parent->m_forceMovement && anyAvailable)
			{
				if (_client->m_id == m_parent->m_currentPlayer)
				{
					m_parent->m_selectedTiles.push_back(_boardTile);

					ImGui::LogCustomConsole("You must jump a highlighted tile to continue.");
				}
			}
		}

		if (!m_parent->m_forceMovement || !anyAvailable)
		{
			for each(BoardTile::MoveInfo* movement in _boardTile->m_neighbors)
			{
				movement->endTile->m_boardColour = movement->endTile->m_boardColourOrigin;
			}

			if (_client->m_id == m_parent->m_currentPlayer)
			{
				_client->SetCurrentPlayer((_client->m_id == 1) ? 2 : 1);
			}
		}
	}
}

void BoardTile::Draw(Camera* _camera)
{
	//Slick looking case to manage the tile drawn
	switch (m_boardColour)
	{
	case BoardColour::BLACK:
	{
		Gizmos::addAABBFilled(m_position, m_scale, glm::vec4(0.4, 0.4, 0.4, 1));
	}break;
	case BoardColour::WHITE:
	{
		Gizmos::addAABBFilled(m_position, m_scale, glm::vec4(1, 1, 1, 1));
	}break;
	case BoardColour::SELECTED:
	{
		Gizmos::addAABBFilled(m_position, m_scale, glm::vec4(1, 0.5, 0, 1));
	}break;
	case BoardColour::AVAILABLE:
	{
		Gizmos::addAABBFilled(m_position, m_scale, glm::vec4(0, 1, 0, 1));
	}break;
	case BoardColour::NONE:
	{
		Gizmos::addAABB(m_position, m_scale, glm::vec4(1, 1, 1, 1));
	}break;
	}
}