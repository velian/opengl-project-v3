#version 410

in vec2 vTexcoord;

out vec4 FragColor;

uniform sampler2D diffuse;
uniform sampler2D overlay;

uniform bool HasTexture;

void main()
{
	//FragColor = texture(diffuse, vec2(vTexcoord.x, vTexcoord.y));

	if(HasTexture)
	{
		FragColor = texture(overlay, vec2(vTexcoord.x, -vTexcoord.y));
	}

	//if(FragColor.a < 0.1f)
	//{
	//	discard;
	//}
}