#include <IMGUI/imgui.h>

#include "Scene/Light.h"

void Light::SetLightDirection(glm::vec3 _lightDirection)
{
	m_lightDirection = _lightDirection;
	CalculateProjection();
}

void Light::CalculateProjection()
{
	if (m_lightDirection != glm::vec3(0, 0, 0))
	{
		m_lightProjection = glm::ortho<float>(-10, 10, -10, 10, -10, 10);
		m_lightView = glm::lookAt(m_lightDirection, glm::vec3(0), glm::vec3(0, 1, 0));

		m_lightMatrix = m_lightProjection * m_lightView;
	}
	else
	{
		printf("Setup Direction Is glm::vec3( 0, 0, 0 ) : Light Setup Failed");
	}
}

void Light::DrawGUI(char* _lightName)
{
	if (ImGui::TreeNode(_lightName))
	{
		float lightColour[3] = { m_lightColour.x, m_lightColour.y, m_lightColour.z };
		//float shadowColour[3] = { m_shadowColour.x, m_shadowColour.y, m_shadowColour.z };
		float lightDirection[3] = { m_lightDirection.x, m_lightDirection.y, m_lightDirection.z };

		ImGui::SliderFloat3("Direction", lightDirection, -2, 2, "%.3f", 0.5f);
		ImGui::ColorEdit3("Colour", lightColour);
		//ImGui::ColorEdit3("S - Colour", shadowColour);
		
		m_lightDirection.xyz = glm::vec3(lightDirection[0], lightDirection[1], lightDirection[2]);
		//m_shadowColour.xyz = glm::vec3(shadowColour[0], shadowColour[1], shadowColour[2]);
		m_lightColour.xyz = glm::vec3(lightColour[0], lightColour[1], lightColour[2]);

		ImGui::TreePop();
	}
}