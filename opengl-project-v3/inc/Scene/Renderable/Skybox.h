#ifndef SKYBOX_H
#define SKYBOX_H

#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <vector>

struct ShaderData;

class Camera;
class Texture;
class FreeCamera;
class Skybox
{
public:

	Skybox(std::vector<std::string> _filePaths);

	void Draw(Camera* _camera);

private:

	void Create();

	unsigned int m_programID;
	
	ShaderData* m_shaderData;
	Texture* m_texture;
};

#endif