#ifndef NETWORK_H
#define NETWORK_H

#define	DEFAULT_PORT 3180
#define	DEFAULT_IP "127.0.0.1"
#define DEFAULT_NAME "Player"
#define MAX_CLIENTS 16

#include <vector>
#include <IMGUI/ImGui.h>
#include <RAKNET/RakPeerInterface.h>
#include <RAKNET/MessageIdentifiers.h>
#include <RAKNET/BitStream.h>
#include <RAKNET/RakNetTypes.h>

using namespace RakNet;
class Network
{
public:

	Network();
	~Network();

	enum ServerMessages
	{
		ID_CLIENT_CONNECT = ID_USER_PACKET_ENUM + 1,
		ID_CLIENT_UPDATE,
		ID_CLIENT_SET_CURRENT_PLAYER,
		ID_CLIENT_REMOVE_PIECE,
		ID_CLIENT_MOVE_PIECE,
		ID_CLIENT_SEND_REMOVE_ORDER,
		ID_CLIENT_SEND_MOVE_ORDER,
		ID_CLIENT_MESSAGE,
		ID_SERVER_BROADCAST,
		ID_SERVER_UPDATE_CLIENT_LIST
	};

	void EndConnection();

	bool Initialized(){ return m_initialized; }

	virtual void Update();

	char* m_clientName;
	std::vector<char*> m_clientList;

protected:

	bool m_isServer;
	bool m_initialized;
	bool m_running;

	RakPeerInterface* m_peer = RakPeerInterface::GetInstance();
	Packet* m_packet;
	SystemAddress m_systemAddress;

	unsigned int m_maxClients;
	unsigned int m_port;
	char* m_ipAddress;
};

#endif NETWORK_H