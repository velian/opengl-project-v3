#include <GL/gl_core_4_4.h>
#include <GLFW/glfw3.h>
#include <IMGUI/imgui.h>

#include "Scene/Renderable/Object.h"
#include "Scene/Camera/Camera.h"
#include "Scene/Light.h"

#include "Utility/Texture.h"

#include <GLM/glm.hpp>

Object::Object(char* _filePath, std::vector<char*> _texturePaths)
{
	m_data = ObjectLoader::LoadObject(_filePath);
	m_transform = glm::mat4();
	m_transform[0][0] = 0.01f;
	m_transform[1][1] = 0.01f;
	m_transform[2][2] = 0.01f;
	m_transform[3][0] = 0;
	m_transform[3][1] = 0;
	m_transform[3][2] = 0;

	if (!_texturePaths.empty())
	{
		for each (char* texPath in _texturePaths)
			m_textures.push_back(new Texture(texPath));
	}
}

void Object::Initialize(unsigned int _programID)
{
	m_shaderData = new ShaderData();
	m_perlinTexture = NULL;

	m_layerVisualizer = false;
	m_perlinSeed = 8;
	m_octaves = 3;
	m_scale = 3;
	m_amplitude = 4;
	m_persistance = 4.5;

	m_rotationSpeed = 0.1f;

	if (_programID != NULL)
	{
		m_programID = _programID;
		Create(_programID);
	}
	else
	{
		printf("Tried to initialize object with NULL shader\nLoaded default instead\n");
		Create(_programID);
	}
	
}

void Object::Create(unsigned int _programID)
{
	if (_programID == NULL)
	{
		std::vector<std::string> shaders;
		shaders.push_back("./shaders/object/objectDefault.vs");
		shaders.push_back("./shaders/object/objectDefault.fs");

		m_programID = ShaderLoader::GetInstance()->LoadShaders(shaders);

		printf("Initialized Object as Default");
	}

	for (unsigned int i = 0; i < m_data->shapes.size(); ++i)
	{
		glGenVertexArrays(1, &m_shaderData[i].m_VAO);
		glGenBuffers(1, &m_shaderData[i].m_VBO);
		glGenBuffers(1, &m_shaderData[i].m_IBO);
		glBindVertexArray(m_shaderData[i].m_VAO);

		unsigned int floatCount = m_data->shapes[i].mesh.positions.size();
		floatCount += m_data->shapes[i].mesh.normals.size();
		floatCount += m_data->shapes[i].mesh.texcoords.size();

		std::vector<float> vertexData;
		vertexData.reserve(floatCount);

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.positions.begin(),
			m_data->shapes[i].mesh.positions.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.normals.begin(),
			m_data->shapes[i].mesh.normals.end());

		vertexData.insert(vertexData.end(),
			m_data->shapes[i].mesh.texcoords.begin(),
			m_data->shapes[i].mesh.texcoords.end());

		m_data[i].m_indexCount = m_data->shapes[i].mesh.indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, m_shaderData[i].m_VBO);
		glBufferData(GL_ARRAY_BUFFER,
			vertexData.size() * sizeof(float),
			vertexData.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_shaderData[i].m_IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_data->shapes[i].mesh.indices.size() * sizeof(unsigned int),
			m_data->shapes[i].mesh.indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (void*)(sizeof(float)*m_data->shapes[i].mesh.positions.size()));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float)*(m_data->shapes[i].mesh.positions.size() + (m_data->shapes[i].mesh.normals.size()))));

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}

void Object::InitializePerlin(int _seed, int _octaves, float _scale, float _amplitude, float _persistance)
{
	int dims = 64;
	float* perlinData = new float[dims * dims];
	float scale = (1.0f / dims) * _scale;
	int octaves = _octaves;
	for (int x = 0; x < dims; x++)
	{
		for (int y = 0; y < dims; y++)
		{
			float amplitude = _amplitude;
			float persistance = _persistance;

			perlinData[y * dims + x] = 0;

			for (int o = 0; o < octaves; o++)
			{
				float freq = powf(2, (float)o);
				float perlinSample = glm::perlin(glm::vec3((float)x, (float)y, _seed) * scale * freq) * 0.5f + 0.5f;

				perlinData[y * dims + x] += perlinSample * amplitude;
				amplitude *= persistance;
			}
		}
	}

	glGenTextures(1, &m_perlinTexture);
	glBindTexture(GL_TEXTURE_2D, m_perlinTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlinData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Object::Update(double _deltaTime)
{
	for each(Texture* tex in m_textures)
	{
		tex->Update(_deltaTime);
	}

	glm::mat4 rotMat = glm::rotate(glm::pi<float>() * m_rotationSpeed * (float)_deltaTime, glm::vec3(0, 1, 0));
	m_transform = m_transform * rotMat;
}

void Object::Draw(Camera* _camera, Light* _light)
{
	glUseProgram(m_programID);

	int location = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(location, 1, GL_FALSE, (float*)&_camera->getProjectionView()[0][0]);

	location = glGetUniformLocation(m_programID, "Transform");
	glUniformMatrix4fv(location, 1, GL_FALSE, &m_transform[0][0]);

	if (m_textures.empty() != true)
	{
		location = glGetUniformLocation(m_programID, "HasTextures");
		glUniform1i(location, true);		

		for (unsigned int i = 0; i < m_textures.size(); i++)
		{
			char formatTex[32];

			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, m_textures[i]->m_id);

			sprintf(formatTex, "TextureList[%i]", i);

			location = glGetUniformLocation(m_programID, formatTex);
			glUniform1i(location, i);
		}
	}

	if (m_perlinTexture != NULL)
	{
		if (m_layerVisualizer)
		{
			location = glGetUniformLocation(m_programID, "LayerVisualizer");
			glUniform1i(location, true);
		}
		else
		{
			location = glGetUniformLocation(m_programID, "LayerVisualizer");
			glUniform1i(location, false);
		}

		location = glGetUniformLocation(m_programID, "HasPerlin");
		glUniform1i(location, true);

		glActiveTexture(GL_TEXTURE0 + m_textures.size());
		glBindTexture(GL_TEXTURE_2D, m_perlinTexture);

		location = glGetUniformLocation(m_programID, "perlinTexture");
		glUniform1i(location, m_textures.size());
	}	

	if (_light != nullptr)
	{
		location = glGetUniformLocation(m_programID, "HasLight");
		glUniform1i(location, true);

		location = glGetUniformLocation(m_programID, "LightColour");
		glUniform3f(location, _light->m_lightColour.x, _light->m_lightColour.y, _light->m_lightColour.z);

		location = glGetUniformLocation(m_programID, "LightDir");
		glUniform3f(location, _light->m_lightDirection.x, _light->m_lightDirection.y, _light->m_lightDirection.z);
	}
	else
	{
		printf("Object with null pointer light drawing\n");
	}

	for (unsigned int i = 0; i < m_data->m_objectShapeSize; i++)
	{
		glBindVertexArray(m_shaderData[i].m_VAO);
		glDrawElements(GL_TRIANGLES, m_data->m_indexCount, GL_UNSIGNED_INT, 0);
	}

	glUseProgram(0);
}

void Object::DrawGUI(char* _objectName)
{
	if (ImGui::TreeNode(_objectName)) //If the treenode is open
	{
		if (ImGui::TreeNode("Transform")) //And if this tree node is open
		{
			float pos[] =		{ m_transform[3][0], m_transform[3][1], m_transform[3][2] };
			float scale[] =		{ m_transform[0][0], m_transform[1][1], m_transform[2][2] };

			ImGui::InputFloat3("Position", pos);
			ImGui::InputFloat3("Scale", scale);

			ImGui::InputFloat("Rotation Speed", &m_rotationSpeed);

			m_transform[3][0] = pos[0];
			m_transform[3][1] = pos[1];
			m_transform[3][2] = pos[2];

			m_transform[0][0] = scale[0];
			m_transform[1][1] = scale[1];
			m_transform[2][2] = scale[2];

			ImGui::TreePop();
		}

		if (ImGui::TreeNode("Perlin Options"))
		{
			ImGui::InputInt("Seed", &m_perlinSeed);
			ImGui::InputInt("Octaves", &m_octaves);
			ImGui::InputFloat("Scale", &m_scale, 1.f, 10.f);
			ImGui::InputFloat("Amplitude", &m_amplitude, 1.f, 10.f);
			ImGui::InputFloat("Peristance", &m_persistance, 1.f, 10.f);
			ImGui::Checkbox("Visualize Layers", &m_layerVisualizer);

			if (ImGui::Button("Generate Perlin"))
			{
				InitializePerlin(m_perlinSeed, m_octaves, m_scale, m_amplitude, m_persistance);
			}

			ImGui::TreePop();
		}
		ImGui::TreePop();
	}
}