#include "./Utility/Network.h"

Network::Network()
{
	m_maxClients = MAX_CLIENTS;
	m_port = DEFAULT_PORT;
	m_ipAddress = DEFAULT_IP;
	m_clientName = DEFAULT_NAME;
	m_initialized = false;
}

Network::~Network()
{
	EndConnection();
	m_peer->DeallocatePacket(m_packet);
}

void Network::EndConnection()
{
	if (!m_initialized) return;

	m_running = false;
}

void Network::Update()
{
	//Inherited function
}