#pragma once

#include <vector>
#include "Utility/Network.h"

using namespace RakNet;
class Server : public Network
{
public:

	Server();
	~Server();

	void InitializeServer();
	void InitializeServer(unsigned int _port, char* _ipAddress);

	virtual void Update();

	void SendServerMessage(char* _serverMessage);

private:

	void UpdateServer();
	
};