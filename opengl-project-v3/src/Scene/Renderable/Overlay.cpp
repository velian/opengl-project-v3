#include "Scene/Renderable/Overlay.h"
#include "Utility/Texture.h"
#include "Utility/ShaderLoader.h"

Overlay::Overlay(std::string _texturePath)
{
	m_texture = new Texture(_texturePath);

	Create();
}

Overlay::Overlay(Texture* _texture)
{
	m_texture = _texture;
}

void Overlay::Update()
{

}

void Overlay::Draw(Camera* _camera)
{
	if (m_programID == NULL)
	{
		printf("Tried to render Overlay with null program ID\nSetting to default program\n");

		//Create default shaders in case we screw up
		std::vector<std::string> defaultShaders;
		defaultShaders.push_back("./shaders/renderTarget/renderTargetOverlay.vs");
		defaultShaders.push_back("./shaders/renderTarget/renderTargetOverlay.fs");

		m_programID = ShaderLoader::GetInstance()->LoadShaders(defaultShaders);
	}

	glUseProgram(m_programID);

	if (m_FBOTexture != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_FBOTexture);
		glUniform1i(glGetUniformLocation(m_programID, "diffuse"), 0);
	}

	if (m_texture != nullptr){
		int location = glGetUniformLocation(m_programID, "HasTexture");
		glUniform1i(location, true);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_texture->GetID());
		glUniform1i(glGetUniformLocation(m_programID, "overlay"), 1);
	}

	glBindVertexArray(m_shaderData->m_VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
	glUseProgram(0);
}