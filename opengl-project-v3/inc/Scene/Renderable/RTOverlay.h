#ifndef RTOVERLAY_H
#define RTOVERLAY_H

#include "Utility/RenderTarget.h"

class Texture;
class RTOverlay : public RenderTarget
{
public:

	RTOverlay();

	virtual void Update();
	virtual void Draw(Camera* _camera);

};

#endif OVERLAY_H