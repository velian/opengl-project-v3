#version 410

layout(location=0) in vec3 Position;
layout(location=1) in vec3 Normal;
layout(location=2) in vec2 UV;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vUV;

uniform mat4 Transform;
uniform mat4 ProjectionView;

void main()
{
	vPosition = Position;
	vNormal = Normal.xyz;
	vUV = UV;

	gl_Position = ProjectionView * Transform * vec4(Position, 1);
}