#include "Application.h"

int main(int argc, char **argv)
{
	Application::GetInstance()->Initialize(argc, argv, 1280, 720, "opengl-project-v3");
	Application::GetInstance()->Run();
	return 0;
}