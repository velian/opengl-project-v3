#version 410

in vec3 gPosition;
in vec3 gNormal;
in vec2 gUV;

out vec4 FragColor;

uniform mat4 Transform;

uniform bool LayerVisualizer;

uniform bool HasTextures;
uniform sampler2D TextureList[4];

uniform bool HasLight;
uniform vec3 LightDir; 
uniform vec3 LightColour;

uniform bool HasPerlin;
uniform sampler2D perlinTexture;

void main()
{
	if(HasTextures && !LayerVisualizer)
	{
		vec3 origin = vec3(Transform[3][0], Transform[3][1], Transform[3][2]);

		float distanceFromOrigin = length(gPosition - origin);

		if(distanceFromOrigin < 200)
		{
			FragColor = texture(TextureList[0], gUV * 3);
		}
		else if(distanceFromOrigin < 225)
		{
			FragColor = texture(TextureList[1], gUV * 3);
		}
		else if(distanceFromOrigin < 250)
		{
			FragColor = texture(TextureList[2], gUV * 3);
		}
		else
		{
			FragColor = texture(TextureList[0], gUV * 3);
		}
	}
	else
	{
		vec3 origin = vec3(Transform[3][0], Transform[3][1], Transform[3][2]);

		float distanceFromOrigin = length(gPosition - origin);

		if(distanceFromOrigin < 200)
		{
			FragColor = vec4(1,0,0,1);
		}
		else if(distanceFromOrigin < 225)
		{
			FragColor = vec4(0,1,0,1);
		}
		else if(distanceFromOrigin < 250)
		{
			FragColor = vec4(0,0,1,1);
		}
		else
		{
			FragColor = vec4(1,1,1,1);
		}
	}
	
	if(HasLight)
	{
		float d = max(0, dot(normalize(gNormal.xyz), LightDir));
		FragColor *= vec4(LightColour * d, 1);
	}	
}